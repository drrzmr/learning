println("week1 test worksheet")

val f1: String => String = {
  case "ping" => "pong"
}
f1("ping")
// f1("abc") // -> MatchError

val f2: PartialFunction[String, String] = {
  case "ping" => "pong"
}

if (f2.isDefinedAt("ping")) f2("ping")
if (f2.isDefinedAt("abc")) f2("abc")