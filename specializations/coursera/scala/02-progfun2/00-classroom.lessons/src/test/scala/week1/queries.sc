import week1.Database


Database.findTitleByAuthor("Bird,").foreach(title => {
  println(s"title: $title")
})

Database.findTitleByAuthor2("Bird,").foreach(title => {
  println(s"title: $title")
})

Database.findBooksByTitle("Programs").foreach(title => {
  println(s"title: $title")
})

Database.findAuthorsWithTwoBooks.foreach(author => {
  println(s"author: $author")
})
