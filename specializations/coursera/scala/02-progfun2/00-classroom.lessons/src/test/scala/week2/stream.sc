def isPrime(n: Int): Boolean =
  if (n < 2) false
  else (2 until n).forall(n % _ != 0)


def streamRange(lo: Int, hi: Int): Stream[Int] =
  if (lo >= hi) Stream.empty
  else Stream.cons(lo, streamRange(lo + 1, hi))

((1000 to 10000) filter isPrime) (1)
((1000 to 10000).toStream filter isPrime) (1)

streamRange(1, 10).take(3).toList

def from(n: Int): Stream[Int] = n #:: from(n + 1)

val nats = from(0)
val m4s = nats.map(_ * 4)
m4s
  .take(100)
  .toList
  .mkString(", ")

def sieve(s: Stream[Int]): Stream[Int] =
  s.head #:: sieve(s.tail filter(_ % s.head != 0))

val primes = sieve(from(2))
primes.take(100).toList.mkString(", ")

def sqrtStream(x: Double): Stream[Double] = {
  def improve(guess: Double): Double = (guess + x / guess ) / 2
  lazy val guesses: Stream[Double] = 1 #:: guesses.map(improve)
  guesses
}

def isGoodEnough(guess: Double, x: Double) =
  math.abs((guess * guess - x) / x) < 0.0001

sqrtStream(4)
  .take(20)
  .toList

sqrtStream(4)
  .filter(isGoodEnough(_, 4))
  .take(20)
  .toList

