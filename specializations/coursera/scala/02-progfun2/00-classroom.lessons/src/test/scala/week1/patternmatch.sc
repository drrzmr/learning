println("Pattern Match Worksheet")

val f1: String => String = {
  case "ping" => "pong"
}
f1("ping")
// f1("abc") // -> MatchError

val f2: PartialFunction[String, String] = {
  case "ping" => "pong"
}

if (f2.isDefinedAt("ping")) f2("ping")
if (f2.isDefinedAt("abc")) f2("abc")

val f: PartialFunction[List[Int], String] = {
  case Nil => "one"
  case x :: y :: rest => "two"
}

f.isDefinedAt(List(1, 2, 3)) // true

val g: PartialFunction[List[Int], String] = {
  case Nil => "one"
  case x :: rest => rest match {
    case Nil => "two"
  }
}

g.isDefinedAt(List(1, 2, 3)) // true
