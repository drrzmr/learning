object ForTranslation {

  def pairs(limit: Int): Seq[(Int, Int)] = {
    for {
      i <- 1 until limit
      j <- 1 until i
      if isPrime(i + j)
    } yield (i, j)
  }

  def pairs2(limit: Int): Seq[(Int, Int)] = {

    1.until(limit).flatMap(a =>
      1.until(a)
        .withFilter(b => isPrime(a + b))
        .map(b => (a, b))
    )
  }

  def isPrime(n: Int): Boolean = {

    def loop(i: Int): Boolean = {
      if (n == i) true
      else if (n % i == 0) false
      else loop(i + 1)
    }

    if (n < 2) false
    else loop(2)
  }

  def isPrime2(n: Int): Boolean = {
    if (n < 2) false
    else (2 until n).forall(n % _ != 0)
  }
}

ForTranslation.isPrime(1)
ForTranslation.isPrime(2)
ForTranslation.isPrime(3)
!ForTranslation.isPrime(4)
ForTranslation.isPrime(5)

ForTranslation.isPrime2(1)
ForTranslation.isPrime2(2)
ForTranslation.isPrime2(3)
!ForTranslation.isPrime2(4)
ForTranslation.isPrime2(5)

ForTranslation.pairs(3)
ForTranslation.pairs2(3)