package quickcheck

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._


abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
      elem <- arbitrary[A]
      heap <- oneOf(const(empty), genHeap)
    } yield insert(elem, heap)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("add the minimal element, and then finding it, should return the element in question") =
    forAll { (h: H) =>
      val m = if (isEmpty(h)) 0 else findMin(h)
      findMin(insert(m, h)) == m
    }

  property("add a single element to an empty heap, and then removing this element, should yield the element in question") =
    forAll { elem: A =>
      val h = insert(elem, empty)
      findMin(h) == elem
    }

  property("insert any two elements into an empty heap, find the minimum of the resulting heap should get the smallest of the two elements back") =
    forAll { (a: A, b: A) =>
      findMin(
        insert(b,
          insert(a, empty))) == math.min(a, b)
    }

  property("insert an element into an empty heap, then delete the minimum, the resulting heap should be empty") =
    forAll { a: A =>
      isEmpty(
        deleteMin(
          insert(a, empty)))
  }

  property("given any heap, you should get a sorted sequence of elements") =
    forAll { heap: H => isSorted(heap) }


  property("finding a minimum of the melding of any two heaps should return a minimum of one or the other") =
    forAll { (heap1: H, heap2: H) =>
      findMin(meld(heap1, heap2)) == math.min(findMin(heap1), findMin(heap2))
    }

  property("meld | must be equal two different heaps with same elements") =
    forAll { (heap1: H, heap2: H) =>
      isEqual(meld(heap1, heap2), meld(deleteMin(heap1), insert(findMin(heap1), heap2)))
    }
  property("meld | must be equal with different order") =
    forAll { (heap1: H, heap2: H) => isEqual(meld(heap1, heap2), meld(heap2, heap1)) }

  property("meld | must works with empty on right") =
    forAll { heap: H => isEqual(meld(heap, empty), meld(empty, heap)) }

  property("meld | must works with empty on left") =
    forAll { heap: H => isEqual(meld(empty, heap), meld(heap, empty)) }

  property("extra | meld works with empty on both side") =
    forAll { _: H => isEmpty(meld(empty, empty)) }

  property("meld | must be sorted 1") =
    forAll { (heap1: H, heap2: H) => isSorted(meld(heap1, heap2)) }

  property("meld | must be sorted 2") =
    forAll { (heap1: H, heap2: H) => isSorted(meld(heap2, heap1)) }

  def isEqual(heap1: H, heap2: H): Boolean = {

    @scala.annotation.tailrec
    def loop(a: H, b: H): Boolean =
    if (isEmpty(a) && isEmpty(b)) true
    else
      if (findMin(a) != findMin(b)) false
      else loop(deleteMin(a), deleteMin(b))

    loop(heap1, heap2)
  }


  def isSorted(heap: H): Boolean = {

    @scala.annotation.tailrec
    def loop(heap: H, elem: A): Boolean = {
      if (isEmpty(heap)) true
      else {
        val e = findMin(heap)
        if (elem > e) false
        else loop(deleteMin(heap), e)
      }
    }

    isEmpty(heap) || loop(deleteMin(heap), findMin(heap))
  }
}

object QuickCheck