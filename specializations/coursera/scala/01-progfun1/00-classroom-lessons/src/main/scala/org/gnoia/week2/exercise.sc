object exercise {
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)
  }
  product(x => x * x)(3, 4)


  def fact(n: Int): Int = product(n => n)(1, n)
  fact(4)

  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
    if (a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a+1, b))

  def fact2(n: Int): Int = mapReduce(x => x, (i, j) => i * j, 1)(1, n)
  fact2(4)
  
}