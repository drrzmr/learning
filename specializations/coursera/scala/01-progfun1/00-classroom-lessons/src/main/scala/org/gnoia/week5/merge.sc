//package org.gnoia.week5

object mergesort {

  def msort[T](list: List[T])(implicit ord: Ordering[T]): List[T] = {

    def merge(left: List[T], right: List[T]): List[T] = (left, right) match {
      case (Nil, ys) => ys
      case (xs, Nil) => xs
      case (x :: xs, y :: ys) =>
        if (ord.lt(x, y)) x :: merge(xs, right)
        else y :: merge(left, ys)
    }

    val n = list.length / 2
    if (n == 0) list
    else {
      val (left, right) = list.splitAt(n)
      merge(msort(left), msort(right))
    }
  }


  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("abacaxi", "banana", "xuxu", "amora", "manga")
  msort(nums)
  msort(fruits)
}