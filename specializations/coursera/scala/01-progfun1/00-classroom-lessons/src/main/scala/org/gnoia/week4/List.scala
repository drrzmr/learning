package org.gnoia.week4

trait List[T] {
  def isEmpty: Boolean

  def head: T

  def tail: List[T]
}

class Nil[T] extends List[T] {

  override def isEmpty: Boolean = true

  override def head: T = throw new NoSuchElementException("Nil.head")

  override def tail: List[T] = throw new NoSuchElementException("Nil.tail")
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {

  override def isEmpty: Boolean = false
}

object List {
  // List(x1, x2)
  def apply[T](x1: T, x2: T): List[T] = new Cons(x1, new Cons(x2, new Nil))

  def apply[T]: List[T] = new Nil[T]
}
