import math.abs

object sqrt {

  val tolerance = 0.0001

  def isCloseEnough(x: Double, y: Double) = abs((x - y) / x) / x < tolerance

  def fixedPoint(f: Double => Double)(guess: Double) = {

    def loop(guess: Double): Double = {
      println(guess)
      val next = f(guess)
      if (isCloseEnough(guess, next)) next
      else loop(next)
    }
    loop(guess)
  }
  fixedPoint(x => 1 + x/2)(1)

  // not converge
  //def sqrt(x: Double) = fixedPoint(y => x/y)(1)

  def sqrt(x: Double) = fixedPoint(y => (y+x/y)/2)(1)

  def averageDamp(f: Double => Double)(x: Double) = (x + f(x)) / 2

  def sqrt2(x: Double) = fixedPoint(averageDamp(y => x/y))(1)

  sqrt(2)
  sqrt(4)

  sqrt2(2)
  sqrt2(4)
}