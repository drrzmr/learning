import org.scalatest._

class HelloSpec extends FunSuite with DiagrammedAssertions {
  test("Hello shoud start with H") {
    assert("Hello".startsWith("H"))
  }
}
