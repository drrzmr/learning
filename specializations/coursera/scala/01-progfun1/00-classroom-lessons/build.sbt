ThisBuild / scalaVersion := "2.12.6"
ThisBuild / organization := "org.gnoia"
ThisBuild / version := "0.0.1"


val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
val gigahorse = "com.eed3si9n" %% "gigahorse-okhttp" % "0.3.1"
val playJson = "com.typesafe.play" %% "play-json" % "2.6.10"

lazy val root = (project in file("."))
  .aggregate(core)
  .dependsOn(core)
  .enablePlugins(JavaAppPackaging)
  .settings(
    name := "Progfun",
    libraryDependencies += scalaTest % Test,

  )

lazy val core = (project in file("core"))
  .settings(
    name := "Progfun core",
    libraryDependencies ++= Seq(gigahorse, playJson),
    libraryDependencies += scalaTest % Test,
  )
