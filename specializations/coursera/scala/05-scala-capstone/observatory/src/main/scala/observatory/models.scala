package observatory

import java.time.LocalDate

/**
  * Introduced in Week 1. Represents a location on the globe.
  * @param lat Degrees of latitude, -90 ≤ lat ≤ 90
  * @param lon Degrees of longitude, -180 ≤ lon ≤ 180
  */
case class Location(lat: Double, lon: Double) {

  val asRadians: RadiansLocation = RadiansLocation(
    lat = lat.toRadians,
    lon = lon.toRadians
  )

}

case class RadiansLocation(lat: Double, lon: Double) {

  def deltaLon(other: RadiansLocation): Double = math.abs(lon - other.lon)
  val latSin: Double                           = math.sin(lat)
  val latCos: Double                           = math.cos(lat)

}

object Location {
  def from(lat: String, lon: String): Location = Location(lat.toDouble, lon.toDouble)

  def fromPoint(x: Int, y: Int): Location = Location(
    lat = 90 - y * (180d / height),
    lon = x * (360d / width) - 180
  )
}

/**
  * Introduced in Week 3. Represents a tiled web map tile.
  * See https://en.wikipedia.org/wiki/Tiled_web_map
  * Based on http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
  * @param x X coordinate of the tile
  * @param y Y coordinate of the tile
  * @param zoom Zoom level, 0 ≤ zoom ≤ 19
  */
case class Tile(x: Int, y: Int, zoom: Int)

/**
  * Introduced in Week 4. Represents a point on a grid composed of
  * circles of latitudes and lines of longitude.
  * @param lat Circle of latitude in degrees, -89 ≤ lat ≤ 90
  * @param lon Line of longitude in degrees, -180 ≤ lon ≤ 179
  */
case class GridLocation(lat: Int, lon: Int)

/**
  * Introduced in Week 5. Represents a point inside of a grid cell.
  * @param x X coordinate inside the cell, 0 ≤ x ≤ 1
  * @param y Y coordinate inside the cell, 0 ≤ y ≤ 1
  */
case class CellPoint(x: Double, y: Double)

/**
  * Introduced in Week 2. Represents an RGB color.
  * @param red Level of red, 0 ≤ red ≤ 255
  * @param green Level of green, 0 ≤ green ≤ 255
  * @param blue Level of blue, 0 ≤ blue ≤ 255
  */
case class Color(red: Int, green: Int, blue: Int)

case class StationID(stn: Option[String], wban: Option[String])

object StationID {

  def from(stn: String, wban: String): StationID =
    StationID(
      stn = if (stn.isEmpty) None else Some(stn),
      wban = if (wban.isEmpty) None else Some(wban)
    )
}

object Temperature {

  def fromFahrenheit(fahrenheit: String): Temperature = (fahrenheit.toDouble - 32) / 1.8d
}

object Date {

  def from(year: Year, month: String, day: String): LocalDate = LocalDate.of(year, month.toInt, day.toInt)
}

case class TempAcc(count: Int, temp: Temperature) {
  def inc(other: Temperature): TempAcc = TempAcc(count + 1, temp + other)
  def inc(other: TempAcc): TempAcc     = TempAcc(count + other.count, temp + other.temp)
}

object TempAcc {
  val empty: TempAcc = TempAcc(0, 0d)
}

case class IDWAcc(n: Double, d: Double) {
  def combine(other: IDWAcc): IDWAcc                   = IDWAcc(n + other.n, d + other.d)
  def inc(weighted: Double, temp: Temperature): IDWAcc = IDWAcc(n + weighted * temp, d + weighted)
}

object IDWAcc {
  val empty: IDWAcc = IDWAcc(0d, 0d)
}
