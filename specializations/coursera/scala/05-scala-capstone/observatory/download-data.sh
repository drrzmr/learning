#!/usr/bin/env bash

set -eu

readonly DEBUG=${DEBUG:-"no"}
readonly baseUrl="http://alaska.epfl.ch/files"
readonly destDir="src/main"
readonly fileName="scala-capstone-data.zip"
readonly url="${baseUrl}/${fileName}"

[[ ${DEBUG} != "no" ]] && set -x

cd "${destDir}"
curl "${url}" | jar -x
