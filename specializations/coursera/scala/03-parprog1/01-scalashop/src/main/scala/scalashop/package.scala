import java.util.concurrent._
import scala.util.DynamicVariable

import org.scalameter._

package object scalashop extends BoxBlurKernelInterface {

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA = Int

  /** Returns the red component. */
  def red(c: RGBA): Int = (0xff000000 & c) >>> 24

  /** Returns the green component. */
  def green(c: RGBA): Int = (0x00ff0000 & c) >>> 16

  /** Returns the blue component. */
  def blue(c: RGBA): Int = (0x0000ff00 & c) >>> 8

  /** Returns the alpha component. */
  def alpha(c: RGBA): Int = (0x000000ff & c) >>> 0

  /** Used to create an RGBA value from separate components. */
  def rgba(r: Int, g: Int, b: Int, a: Int): RGBA = {
    (r << 24) | (g << 16) | (b << 8) | (a << 0)
  }

  /** Restricts the integer into the specified range. */
  def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  /** Image is a two-dimensional matrix of pixel values. */
  class Img(val width: Int, val height: Int, private val data: Array[RGBA]) {
    def this(w: Int, h: Int) = this(w, h, new Array(w * h))
    def apply(x: Int, y: Int): RGBA = data(y * width + x)
    def update(x: Int, y: Int, c: RGBA): Unit = data(y * width + x) = c
    override def toString: String = {
      val sb = new StringBuilder("debug \n")
      for (row <- 0 until height) {
        for (col <- 0 until width) {
          sb.append(this.apply(col, row))
          sb.append(",")
        }
        sb.append("\n")
      }
      sb.toString()
    }
  }

  /** Computes the blurred RGBA value of a single pixel of the input image. */
  def boxBlurKernel(src: Img, x: Int, y: Int, radius: Int): RGBA = {
    val colMin = clamp(x - radius, 0, src.width - 1)
    val colMax = clamp(x + radius, 0, src.width - 1)
    val rowMin = clamp(y - radius, 0, src.height - 1)
    val rowMax = clamp(y + radius, 0, src.height - 1)

    var (col, row) = (colMin, rowMin)
    var (r, g, b, a) = (0, 0, 0, 0)
    var count = 0

    while (row <= rowMax) {
      while (col <= colMax) {

        val p = src(col, row)
        r += red(p)
        g += green(p)
        b += blue(p)
        a += alpha(p)

        count += 1
        col += 1
      }
      row += 1
      col = colMin
    }

    val length = (colMax - colMin + 1) * (rowMax - rowMin + 1)
    rgba(r / length, g / length, b / length, a / length)
  }

  def boxBlurKernelX(src: Img, x: Int, y: Int, radius: Int): RGBA = {
    val pixels = for {
      col: Int <- clamp(x - radius, 0, src.width - 1) to clamp(x + radius, 0, src.width - 1)
      row: Int <- clamp(y - radius, 0, src.height - 1) to clamp(y + radius, 0, src.height - 1)
    } yield src(col, row)

    val sum = pixels
      .map(p => (red(p), green(p), blue(p), alpha(p)))
      .foldLeft((0, 0, 0, 0)) {
        case (acc, p) => (acc._1 + p._1, acc._2 + p._2, acc._3 + p._3, acc._4 + p._4)
      }

    rgba(
      sum._1 / pixels.length,
      sum._2 / pixels.length,
      sum._3 / pixels.length,
      sum._4 / pixels.length
    )
  }

  val forkJoinPool = new ForkJoinPool

  abstract class TaskScheduler {
    def schedule[T](body: => T): ForkJoinTask[T]
    def parallel[A, B](taskA: => A, taskB: => B): (A, B) = {
      val right = task {
        taskB
      }
      val left = taskA
      (left, right.join())
    }
  }

  class DefaultTaskScheduler extends TaskScheduler {
    def schedule[T](body: => T): ForkJoinTask[T] = {
      val t = new RecursiveTask[T] {
        def compute: T = body
      }
      Thread.currentThread match {
        case wt: ForkJoinWorkerThread =>
          t.fork()
        case _ =>
          forkJoinPool.execute(t)
      }
      t
    }
  }

  val scheduler =
    new DynamicVariable[TaskScheduler](new DefaultTaskScheduler)

  def task[T](body: => T): ForkJoinTask[T] = {
    scheduler.value.schedule(body)
  }

  def parallel[A, B](taskA: => A, taskB: => B): (A, B) = {
    scheduler.value.parallel(taskA, taskB)
  }

  def parallel[A, B, C, D](taskA: => A, taskB: => B, taskC: => C, taskD: => D): (A, B, C, D) = {
    val ta = task { taskA }
    val tb = task { taskB }
    val tc = task { taskC }
    val td = taskD
    (ta.join(), tb.join(), tc.join(), td)
  }

  // Workaround Dotty's handling of the existential type KeyValue
  implicit def keyValueCoerce[T](kv: (Key[T], T)): KeyValue = {
    kv.asInstanceOf[KeyValue]
  }

  def buildStrips(numElem: Int, numTasks: Int): IndexedSeq[(Int, Int)] = {
    val range = 0 to numElem by math.max(1, numElem / numTasks)
    range.zip(range.tail)
  }
}
