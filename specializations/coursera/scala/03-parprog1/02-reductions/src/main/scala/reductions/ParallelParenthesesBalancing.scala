package reductions

import scala.annotation._
import org.scalameter._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig: MeasureBuilder[Unit, Double] = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime")
    println(s"speedup: ${seqtime.value / fjtime.value}")
  }
}

object ParallelParenthesesBalancing extends ParallelParenthesesBalancingInterface {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    @scala.annotation.tailrec
    def loop(chars: List[Char], acc: Int): Int =
      chars match {
        case Nil => acc
        case head :: tail if head == '(' => loop(tail, acc + 1)
        case head :: tail if head == ')' =>
          val next = acc - 1
          if (next < 0) -1 else loop(tail, next)
        case _ :: tail => loop(tail, acc)
      }

    loop(chars.toList, 0) == 0
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    case class State(open: Int, overflow: Int) {
      def incOpen: State = copy(open = open + 1)
      def decOpen: State = copy(open = open - 1)
      def incOverflow: State = copy(overflow = overflow + 1)
    }

    def traverse(idx: Int, until: Int): State = {

      @scala.annotation.tailrec
      def loop(i: Int, state: State): State = {
        if (i >= until) state
        else {
          loop(i + 1, chars(i) match {
            case '(' => state.incOpen
            case ')' => if (state.open > 0) state.decOpen else state.incOverflow
            case _ => state
          }
          )
        }
      }

      loop(idx, State(0, 0))
    }

    def reduce(from: Int, until: Int): State = {
      val len = until - from

      if (len <= threshold) traverse(from, until)
      else {
        val mid = (len / 2) + from

        val (left, right) = parallel(reduce(from, mid), reduce(mid, until))

        if (left.open > right.overflow) left.copy(open = left.open - right.overflow + right.open)
        else right.copy(overflow = right.overflow - left.open + left.overflow)
      }
    }

    reduce(0, chars.length) == State(0, 0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
