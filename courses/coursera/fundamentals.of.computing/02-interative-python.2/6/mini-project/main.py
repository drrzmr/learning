# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 936x384 - source: jfitz.com
CARD_SIZE = (72, 96)
CARD_CENTER = (36, 48)
card_images = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/cards_jfitz.png")
card_back = simplegui.load_image("http://storage.googleapis.com/codeskulptor-assets/card_jfitz_back.png")

# initialize some useful global variables
in_play = False
outcome = "Blackjack"
msg = "Hit or Stand?"
score = 0

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)


class Hand:
    def __init__(self):
        self.cards = []

    def __str__(self):
        return 'hand: ' + ', '.join([str(card) for card in self.cards])

    def add_card(self, card):
        self.cards.append(card)

    def get_value(self):
        has_aces = False
        acm = 0
        for card in self.cards:
            rank = card.get_rank()
            if rank == 'A':
                has_aces = True

            acm += VALUES[rank]

        if has_aces == True and acm + 10 <= 21:
            return acm + 10
        return acm

    def draw(self, canvas, pos, back = False):
        x, y = pos
        for card in self.cards:
            if False == back:
                card.draw(canvas, (x, y))
            else:
                canvas.draw_image(card_back, CARD_CENTER, CARD_SIZE, [x + CARD_CENTER[0], y + CARD_CENTER[1]], CARD_SIZE)

            x += CARD_SIZE[0] + 5


class Deck:
    def __init__(self):
        self.cards = [Card(suit, rank) for suit in SUITS for rank in RANKS]

    def shuffle(self):
        random.shuffle(self.cards)

    def deal_card(self):
        return self.cards.pop()

    def __str__(self):
        return 'deck: ' + ', '.join([str(card) for card in self.cards])


# buttons
def deal():
    global outcome, msg, in_play
    global deck, player, dealer, score

    if True == in_play:
        in_play = False
        score -= 1
        outcome = 'You quit the game. :/'
        msg = 'Click "Deal" again'
        return

    deck = Deck()
    player = Hand()
    dealer = Hand()

    deck.shuffle()
    player.add_card(deck.deal_card())
    dealer.add_card(deck.deal_card())
    player.add_card(deck.deal_card())
    dealer.add_card(deck.deal_card())

    outcome = 'Welcome to the game. \o/'
    msg = 'Hit or Stand?'
    in_play = True


def hit():
    global in_play, score
    global outcome, msg

    if False == in_play:
        outcome =  'Game is Over!'
        msg = 'Click "Deal"'
        return

    if player.get_value() <= 21:
        player.add_card(deck.deal_card())

    if player.get_value() > 21:
        outcome = 'You have busted. :('
        msg = 'Maybe next?'
        score -= 1
        in_play = False
        return

    outcome = 'Nice! :)'
    msg = 'Hit or Stand?'


def stand():
    global in_play, score
    global outcome, msg

    if False == in_play:
            outcome = 'Game is Over!'
            msg = 'Click "Deal"'
            return

    if player.get_value() > 21:
        outcome = 'You have busted'
        msg = 'Try again...'
        return

    while dealer.get_value() < 17:
        dealer.add_card(deck.deal_card())

    if dealer.get_value() > 21:
        outcome = 'Dealer has busted, you win!'
        msg = 'Try again...' 
        in_play = False
        score += 1
        return

    if dealer.get_value() >= player.get_value():
        outcome = 'You loose. :('
        msg = 'Better luck next time. ;)'
        in_play = False
        score -= 1
        return

    outcome = 'You win!'
    msg = 'One more play?'
    in_play = False
    score += 1
    return


# draw handler
def draw(canvas):
    canvas.draw_text('Blackjack', (20, 50), 50, 'Black')
    canvas.draw_text('score: ' + str(score), (400, 50), 50, ('Black', 'Red')[score < 0])

    canvas.draw_polygon([(100, 520), (500, 520), (500, 580), (100, 580)], 2, 'Black', 'White')
    canvas.draw_text(outcome, (110, 545), 20, 'Black')
    canvas.draw_text('> ' + msg, (110, 570), 18, 'Red')

    dealer.draw(canvas, (20, 100), back = in_play)
    player.draw(canvas, (20, 300), back = False)


    # initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)

deck = None
player = None
dealer = None


# get things rolling
deal()
frame.start()
