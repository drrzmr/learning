'''
Question 7
Convert the following English description into code.

Initialize n to be 1000. Initialize numbers to be a list of numbers from 2 to n, but not including n.
With results starting as the empty list, repeat the following as long as numbers contains any numbers.
Add the first number in numbers to the end of results.
Remove every number in numbers that is evenly divisible by (has no remainder when divided by) the number that you had just added to results.
How long is results?

To test your code, when n is instead 100, the length of results is 25.
'''

def f(n):
    numbers = range(2, n)
    r = []
    i = 0
    while len(numbers) > 0:
        
        first = numbers.pop(0)
        r.append(first)
        
        tmp = []
        for num in numbers:
            if (num % first) != 0:
                tmp.append(num)
        numbers = tmp
    
        i += 1

    return len(r)

print f(100)
# 25

print f(1000)
# 168 
