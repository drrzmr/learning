# Question 8
# How many distinct numbers are printed by the following code? Enter the count.
# 
# def next(x):
#     return (x ** 2 + 79) % 997
# 
# x = 1
# for i in range(1000):
#     print x
#     x = next(x)
# Hint: Consider how editing the code to use a set could help solve the question.

def next(x):
    return (x ** 2 + 79) % 997

s = set([])
x = 1
for i in range(1000):
    s.add(x)
    x = next(x)

print len(s)
# 46
