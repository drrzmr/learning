import simplegui
import random

cards = range(8) + range(8)
print len(cards)
print cards
exposed = []
selected = {}
turns = 0

def new_game():
    global selected, exposed
    random.shuffle(cards)
    exposed = [False for i in range(len(cards))]
    selected = {'a': None, 'b': None}

def mouseclick(pos):
    global turns
    i = pos[0] // 50

    if exposed[i] == True:
        return

    if selected['a'] == None:
        selected['a'] = i
        turns += 1
        exposed[i] = True
    elif selected['b'] == None:
        selected['b'] = i
        exposed[i] = True
    elif cards[selected['a']] != cards[selected['b']]:
        exposed[selected['a']] = False
        exposed[selected['b']] = False
        selected['a'] = i
        turns += 1
        exposed[i] = True
        selected['b'] = None
    elif cards[selected['a']] == cards[selected['b']]:
        selected['a'] = i
        turns += 1
        exposed[i] = True
        selected['b'] = None

def draw(canvas):
    x = 0
    y = 0
    label.set_text('Turns = ' + str(turns))
    for i in range(len(cards)):
        if False == exposed[i]:
            canvas.draw_polygon([(x, y), (x, y+100), (x+50, y+100), (x+50, 0)], 2, 'Red', 'Green')
        else:
            canvas.draw_text(str(cards[i]), (x+15, y+60), 40, 'White')
        x += 50

frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = 0")
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

new_game()
frame.start()
