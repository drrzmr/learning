# Implementation of classic arcade game Pong

import simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True
P_VEL = 5

ball_pos = [WIDTH/2, HEIGHT/2]
ball_vel = [0, 0]
p1_pos = [0, 0]
p2_pos = [0, 0]
p1_vel = 0
p2_vel = 0
points_blue = 0
points_red = 0

# initialize ball_pos and ball_vel for new bal in middle of table
# if direction is RIGHT, the ball's velocity is upper right, else upper left
def spawn_ball(direction):
    global ball_pos, ball_vel
    ball_pos = [WIDTH/2, HEIGHT/2]
    ball_vel = [1, -1]
    ball_vel[0] = random.randrange(120, 240) / 60
    ball_vel[1] = -(random.randrange(60, 180) / 60)

    if direction == RIGHT:
        ball_vel[0] = -ball_vel[0]

# define event handlers
def new_game():
    global p1_pos, p2_pos
    global points_red, points_blue
    points_red = 0
    points_blue = 0
    p1_pos = [0 + HALF_PAD_WIDTH, HEIGHT/2]
    p2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT/2]
    spawn_ball(LEFT)

def top(pos):
    return pos[1] - HALF_PAD_HEIGHT

def bottom(pos):
    return pos[1] + HALF_PAD_HEIGHT

def draw(canvas):
    global score1, score2, p1_pos, p2_pos, ball_pos, ball_vel, points_blue, points_red

    # draw mid line and gutters
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "Red")
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "Blue")

    # update ball
    ball_pos[0] += ball_vel[0]
    ball_pos[1] += ball_vel[1]

    # draw ball
    canvas.draw_circle(ball_pos, BALL_RADIUS, 1, "White", "White")

    # update paddle's vertical position
    p1_pos[1] += p1_vel
    p2_pos[1] += p2_vel

    # keep paddle on the screen
    if top(p1_pos) < 0:
        p1_pos[1] = 0 + HALF_PAD_HEIGHT
    if top(p2_pos) < 0:
        p2_pos[1] = 0 + HALF_PAD_HEIGHT
    if bottom(p1_pos) > HEIGHT:
        p1_pos[1] = HEIGHT - HALF_PAD_HEIGHT
    if bottom(p2_pos) > HEIGHT:
        p2_pos[1] = HEIGHT - HALF_PAD_HEIGHT

    # draw paddles
    x1 = p1_pos[0]
    x2 = p2_pos[0]
    canvas.draw_line((x1, bottom(p1_pos)), (x1, top(p1_pos)), PAD_WIDTH, 'Red')
    canvas.draw_line((x2, bottom(p2_pos)), (x2, top(p2_pos)), PAD_WIDTH, 'Blue')

    # determine whether paddle and ball collide
    # top / bottom
    if (ball_pos[1] + BALL_RADIUS) >= HEIGHT or (ball_pos[1] - BALL_RADIUS) <= 0:
        ball_vel[1] = -ball_vel[1]

    # left
    elif (ball_pos[0] - BALL_RADIUS) <= (0 + PAD_WIDTH):
        # on p1
        if ball_pos[1] >= top(p1_pos) and ball_pos[1] <= bottom(p1_pos):
            ball_vel[0] = -ball_vel[0]
            ball_vel[0] += ball_vel[0] * 0.1
            ball_vel[1] += ball_vel[0] * 0.1
            print ball_vel
        else:
            spawn_ball(LEFT)
            points_blue += 1

    # right
    if (ball_pos[0] + BALL_RADIUS) >= (WIDTH - PAD_WIDTH):
        # on p2
        if ball_pos[1] >= top(p2_pos) and ball_pos[1] <= bottom(p2_pos):
            ball_vel[0] = -ball_vel[0]
            ball_vel[0] += ball_vel[0] * 0.1
            ball_vel[1] += ball_vel[0] * 0.1
            print ball_vel
        else:
            spawn_ball(RIGHT)
            points_red += 1

    # draw scores
    canvas.draw_text(str(points_red), (WIDTH/2 - 100, 50), 50, 'Red')
    canvas.draw_text(str(points_blue), (WIDTH/2 + 100, 50), 50, 'Blue')


def keydown(key):
    global p1_vel, p2_vel, p1_pos
    if key == simplegui.KEY_MAP['w']:
        p1_vel -= P_VEL
    elif key == simplegui.KEY_MAP['s']:
        p1_vel += P_VEL
    elif key == simplegui.KEY_MAP['up']:
        p2_vel -= P_VEL
    elif key == simplegui.KEY_MAP['down']:
        p2_vel += P_VEL


def keyup(key):
    global p1_vel, p2_vel
    if key == simplegui.KEY_MAP['w']:
        p1_vel += P_VEL
    elif key == simplegui.KEY_MAP['s']:
        p1_vel -= P_VEL
    elif key == simplegui.KEY_MAP['up']:
        p2_vel += P_VEL
    elif key == simplegui.KEY_MAP['down']:
        p2_vel -= P_VEL


def button_handler():
    new_game()

# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("Reset", button_handler)

# start frame
new_game()
frame.start()
