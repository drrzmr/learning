import simplegui


tenths = 0
stop_counter = 0
success = 0
running = False


def format(i):
    msec = i % 10
    i = i / 10
    sec = i % 60
    minutes = i / 60
    sec = str(sec)
    if len(sec) == 1:
        sec = "0" + sec
    return str(minutes) + ":" + str(sec) + "." + str(msec)


def start_handler():
    global running
    if running:
        return

    t.start()
    running = True


def stop_handler():
    global stop_counter, tenths, success, running

    if not running:
        return

    stop_counter += 1
    if tenths % 10 == 0:
        success += 1

    t.stop()
    running = False


def reset_handler():
    global tenths, running, success, stop_counter

    if running:
        t.stop()
        running = False

    tenths = 0
    success = 0
    stop_counter = 0


def timer_handler():
    global tenths
    tenths += 1


def draw_handler(canvas):
    canvas.draw_text(format(tenths), (60, 175), 150, "White")
    score = str(success) + "/" + str(stop_counter)
    canvas.draw_text(score, (405, 50), 50, "Green")


f = simplegui.create_frame("Stopwatch", 500, 200)
f.add_button('Start', start_handler, 150)
f.add_button('Stop', stop_handler, 150)
f.add_button('Reset', reset_handler, 150)
f.set_draw_handler(draw_handler)
t = simplegui.create_timer(100, timer_handler)
f.start()
