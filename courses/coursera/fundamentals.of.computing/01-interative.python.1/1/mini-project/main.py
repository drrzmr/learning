from random import randrange

def name_to_number(name):

    if name == 'rock': return 0
    elif name == 'Spock': return 1
    elif name == 'paper': return 2
    elif name == 'lizard': return 3
    elif name == 'scissors': return 4
    else:
        print 'can\'t convert to number this name:' + name
        return None

def number_to_name(number):

    if number == 0: return 'rock'
    elif number == 1: return 'Spock'
    elif number == 2: return 'paper'
    elif number == 3: return 'lizard'
    elif number == 4: return 'scissors'
    else:
        print 'can\'t convert to number this name:' + name
        return None

def rpsls(player_choice): 
    
    print
    player_number = name_to_number(player_choice)
    comp_number = randrange(0, 5)
    comp_choice = number_to_name(comp_number)   
    difference = (comp_number - player_number) % 5

    print 'Player chooses ' + player_choice
    print 'Computer chooses ' + comp_choice
    if difference <= 2:
        print 'Computer wins!'
    else:
        print 'Player wins!'

rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")
