#!/usr/bin/env bash

set -eu

readonly name="smallwiki.db"
readonly databaseDir="enwiki-dataset"
readonly url="https://moocs.scala-lang.org/~dockermoocs/effective-scala/${name}"
readonly output="${databaseDir}/${name}"

command -v wget &> /dev/null || (echo ">>> missing wget command" && exit 1)

mkdir -p ${databaseDir}
wget --continue --quiet ${url} --output-document="${output}"
ls -lh "${output}"
