filename = raw_input('Ender file name: ')
try:
    f = open(filename)
except:
    print 'cant open file: ' + filename

d = {}
for line in f:
    if not line.startswith('From '):
        continue

    sender = line.rstrip().split()[1]
    d[sender] = d.get(sender, 0) + 1

max_key = None
max_value = None
for key, value in d.items():
    if max_key is None:
        max_key = key
        max_value = value
        continue

    if max_value < value:
        max_value = value
        max_key = key

print max_key, max_value
