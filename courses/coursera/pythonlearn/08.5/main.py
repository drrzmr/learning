filename = raw_input('Ender file name: ')
try:
    f = open(filename)
except:
    print 'cant open file: ' + filename

lst = []
for line in f:
    if not line.startswith('From '):
        continue

    line = line.rstrip()
    line = line.split()
    lst.append(line[1])

for i in lst:
    print i

print "There were", len(lst), "lines in the file with From as the first word"
