1. Which of the following commands will create a directory called data in your
current working directory?
 - [ ] pwd ../data
 - [ ] mkdir /Users/data
 - [ ] pwd data
 - [x] mkdir data

2. Which of the following will initiate a git repository locally?
 - [ ] git merge
 - [x] git init
 - [ ] git pull
 - [ ] git push

3. Suppose you have forked a repository called datascientist on Github but it
isn't on your local computer yet. Which of the following is the command to bring
the directory to your local computer? (For this question assume that your user
name is username)
 - [x] git clone https://github.com/username/datascientist.git
 - [ ] git pull https://github.com/username/datascientist.git
 - [ ] git push origin master
 - [ ] git pull

4. Which of the following will create a markdown document with a secondary
heading saying "Data Science Specialization" and an unordered list with the
following for bullet points: Uses R, Nine courses, Goes from raw data to data
products
 - [ ] 
 ``` Markdown
 ## Data Science Specialization

 li Uses R
 li Nine courses
 li Goes from raw data to data products
 ```
 - [ ] 
 ``` Markdown
 ## Data Science Specialization

 * Uses R
 * Nine courses
 * Goes from raw data to data products
 ```
 - [x] 
 ```  Markdown
 *** Data Science Specialization

 * Uses R
 * Nine courses
 * Goes from raw data to data products
 ```
 - [ ] 
 ``` Markdown
 ### Data Science Specialization

 * Uses R 
 * Nine courses 
 * Goes from raw data to data products
 ```
 - [ ] 
 ``` Markdown
 *h2 Data Science Specialization

 * Uses R
 * Nine courses
 * Goes from raw data to data products
 ```

5. Install and load the KernSmooth R package. What does the copyright message
say?
 - [x] Copyright M. P. Wand 1997-2009
 - [ ] Copyright KernSmooth 1997-2009
 - [ ] Copyright M. P. Wand 1997-2013
 - [ ] Copyright Matthew Wand 1997-2009
