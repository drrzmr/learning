1. Suppose I define the following function in R
 ``` R
 cube <- function(x, n) {
         x^3
 }
 ```
 What is the result of running

 ``` R
 cube(3)
 ```
 in R after defining this function?
 - [ ] An error is returned because 'n' is not specified in the call to 'cube'
 - [ ] A warning is given with no value returned.
 - [x] The number 27 is returned
  * Explanation: Because 'n' is not evaluated, it is not needed even though it
    is a formal argument.
 - [ ] The users is prompted to specify the value of 'n'.

2. The following code will produce a warning in R.
 ``` R
 x <- 1:10
 if(x > 5) {
         x <- 0
 }
 ```
 Why?
 - [x] 'x' is a vector of length 10 and 'if' can only test a single logical
       statement.
 - [ ] There are no elements in 'x' that are greater than 5
 - [ ] You cannot set 'x' to be 0 because 'x' is a vector and 0 is a scalar.
 - [ ] The syntax of this R expression is incorrect.
 - [ ] The expression uses curly braces.

3. Consider the following function
 ``` R
 f <- function(x) {
         g <- function(y) {
                 y + z
         }
         z <- 4
         x + g(x)
 }
 ```
 If I then run in R
 ``` R
 z <- 10
 f(3)
 ```
 What value is returned?
 - [ ] 4
 - [x] 10
 - [ ] 7
 - [ ] 16

4. Consider the following expression:
 ``` R
 x <- 5
 y <- if(x < 3) {
         NA
 } else {
         10
 }
 ```
 What is the value of 'y' after evaluating this expression?
 - [ ] 5
 - [ ] NA
 - [ ] 3
 - [x] 10

5. Consider the following R function
 ``` R
 h <- function(x, y = NULL, d = 3L) {
         z <- cbind(x, d)
         if(!is.null(y))
                 z <- z + y
         else
                 z <- z + f
         g <- x + y / z
         if(d == 3L)
                 return(g)
         g <- g + 10
         g
 }
 ```
Which symbol in the above function is a free variable?
 - [x] f
 - [ ] z
 - [ ] d
 - [ ] L
 - [ ] g

6. What is an environment in R?
 - [ ] a list whose elements are all functions
 - [ ] a special type of function
 - [ ] an R package that only contains data
 - [x] a collection of symbol/value pairs

7. The R language uses what type of scoping rule for resolving free variables?
 - [ ] global scoping
 - [x] lexical scoping
 - [ ] dynamic scoping
 - [ ] compilation scoping

8. How are free variables in R functions resolved?
 - [ ] The values of free variables are searched for in the environment in which
       the function was called
 - [x] The values of free variables are searched for in the environment in which
       the function was defined
 - [ ] The values of free variables are searched for in the working directory
 - [ ] The values of free variables are searched for in the global environment

9. What is one of the consequences of the scoping rules used in R?
 - [x] All objects must be stored in memory
 - [ ] All objects can be stored on the disk
 - [ ] R objects cannot be larger than 100 MB
 - [ ] Functions cannot be nested

10. In R, what is the parent frame?
 - [ ] It is the environment in which a function was defined
 - [ ] It is the package search list
 - [ ] It is always the global environment
 - [x] It is the environment in which a function was called
