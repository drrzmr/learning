1. Take a look at the 'iris' dataset that comes with R. The data can be loaded
with the code:
 ``` R
 library(datasets)
 data(iris)
 ```
 A description of the dataset can be found by running
 ``` R
 ?iris
 ```
 There will be an object called 'iris' in your workspace. In this dataset, what
 is the mean of 'Sepal.Length' for the species virginica? (Please only enter the
 numeric result and nothing else.)
 - [x] Answer: 6.588
 To get the answer here, you can use 'tapply' to calculate the mean of
 'Sepal.Length' within each species.

2. Continuing with the 'iris' dataset from the previous Question, what R code
returns a vector of the means of the variables 'Sepal.Length', 'Sepal.Width',
'Petal.Length', and 'Petal.Width'?

 - [ ] colMeans(iris)
 - [ ] rowMeans(iris[, 1:4])
 - [ ] apply(iris, 1, mean)
 - [x] apply(iris[, 1:4], 2, mean)

3. Load the 'mtcars' dataset in R with the following code
 ``` R
 library(datasets)
 data(mtcars)
 ```
 There will be an object names 'mtcars' in your workspace. You can find some
 information about the dataset by running
 ``` R
 ?mtcars
 ```
 How can one calculate the average miles per gallon (mpg) by number of cylinders
 in the car (cyl)?
 - [ ] tapply(mtcars$cyl, mtcars$mpg, mean)
 - [ ] split(mtcars, mtcars$cyl)
 - [x] sapply(split(mtcars$mpg, mtcars$cyl), mean)
 - [ ] apply(mtcars, 2, mean)

4. Continuing with the 'mtcars' dataset from the previous Question, what is the
absolute difference between the average horsepower of 4-cylinder cars and the
average horsepower of 8-cylinder cars?
 - [x] Answer: 126.5779

5. If you run
 ``` R
 debug(ls)
 ```
 what happens when you next call the 'ls' function?
 - [ ] Execution of the 'ls' function will suspend at the 4th line of the
       function and you will be in the browser.
 - [ ] The 'ls' function will return an error.
 - [x] Execution of 'ls' will suspend at the beginning of the function and you
       will be in the browser.
 - [ ] You will be prompted to specify at which line of the function you would
       like to suspend execution and enter the browser.
