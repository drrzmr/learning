#!/usr/bin/env python3

import sys

__symbols__ = {
	'R0': '0',
	'R1': '1',
	'R2': '2',
	'R3': '3',
	'R4': '4',
	'R5': '5',
	'R6': '6',
	'R7': '7',
	'R8': '8',
	'R9': '9',
	'R10': '10',
	'R11': '11',
	'R12': '12',
	'R13': '13',
	'R14': '14',
	'R15': '15',
	'SCREEN': '16384',
	'KBD': '24576',
	'SP': '0',
	'LCL': '1',
	'ARG': '2',
	'THIS': '3',
	'THAT': '4'
}

__dest__ = {
	'null': '000',
	'M': '001',
	'D': '010',
	'MD': '011',
	'A': '100',
	'AM': '101',
	'AD': '110',
	'AMD': '111'
}

__comp__ = {
	'0': '0101010',
	'1': '0111111',
	'-1': '0111010',
	'D': '0001100',
	'A': '0110000',
	'!D': '0001101',
	'!A': '0110001',
	'-D': '0001111',
	'-A': '0110011',
	'D+1': '0011111',
	'A+1': '0110111',
	'D-1': '0001110',
	'A-1': '0110010',
	'D+A': '0000010',
	'D-A': '0010011',
	'A-D': '0000111',
	'D&A': '0000000',
	'D|A': '0010101',
	'M': '1110000',
	'!M': '1110001',
	'-M': '1110011',
	'M+1': '1110111',
	'M-1': '1110010',
	'D+M': '1000010',
	'D-M': '1010011',
	'M-D': '1000111',
	'D&M': '1000000',
	'D|M': '1010101'
}

__jump__ = {
	'null': '000',
	'JGT': '001',
	'JEQ': '010',
	'JGE': '011',
	'JLT': '100',
	'JNE': '101',
	'JLE': '110',
	'JMP': '111'
}

def loadFile(name):
	asm = []
	inp = open(name)
	for line in inp:
		comment_pos = line.find('//')
		if (comment_pos >= 0):
			line = line[0:comment_pos]
		line = line.strip('\n\t ')
		if len(line) == 0:
			continue
		asm.append(line)

	return asm

def parseArgs():
	try:
		_, name = sys.argv
	except:
		print('usage: ' + sys.argv[0] + ' <input file>')
		sys.exit(-1)
	return name

def firstPass(asm):

	r = []
	addr = 0
	for line in asm:
		if line[0] != '(' and line[-1] != ')':
			r.append(line)
			addr += 1
			continue
		symbolName = line[1:-1]
		__symbols__[symbolName] = str(addr)

	return r

def secondPass(asm):

	varAddr = 16
	r = []
	for line in asm:
		if line[0] != '@':
			r.append(line)
			continue

		varName = line[1:]
		if varName.isdecimal():
			r.append(line)
			continue

		if varName not in __symbols__:
			__symbols__[varName] = str(varAddr)
			varAddr += 1

		r.append('@' + __symbols__[varName])

	return r

def translate(asm):

	code = []
	for line in asm:

		# A instruction
		if line[0] is '@':
			#code.append(line[1:].rjust(16, '0'))
			code.append('{0:016b}'.format(int(line[1:])))
			continue

		# C instruction

		# handle dest
		if '=' not in line:
			dest = __dest__['null']
		else:
			regName, line = line.split('=')
			dest = __dest__[regName]

		# handle comp
		if ';' not in line:
			comp = __comp__[line]
			line = None
		else:
			compStr, line = line.split(';')
			comp = __comp__[compStr]	

		# handle jump
		if line:
			jump = __jump__[line]
		else:
			jump = __jump__['null']
	
		code.append('111' + comp + dest + jump)

	return code

if __name__ == '__main__':

	inFileName = parseArgs()
	asm = loadFile(inFileName)
	asm = firstPass(asm)
	asm = secondPass(asm)
	code = translate(asm)

	for line in code:
		print(line)
