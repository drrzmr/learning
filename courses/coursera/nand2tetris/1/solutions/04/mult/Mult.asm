//
// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// int
// mult(int a, int b)
// {
//     int r = 0;
//	   int i = 0;
//
// LOOP:
//     if (i >= b)
//	       goto END;
//
//	   r += b;
//	   i++;
//
//     goto LOOP;
// END:
//	   return r;
// }

	@2      // r
	M = 0   // r = 0
	@i
	M = 0   // i = 0;

(LOOP)

	@i
	D = M
	@1      // b
	D = D-M // if i >= R1

	@END
	D; JGE    // goto END

	@0        // a
	D = M
	@2        // r
	M = D+M   // r = r + a  R2 = R2 + R0

	@i
	M = M+1   // i++

	@LOOP     // goto LOOP
	0; JMP

(END)
	@END
	0; JMP
