#!/usr/bin/python

# Format of each line is:
# date \t time \t store name \t item description \t cost \t method of payment

import sys

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) == 6:
        date, time, store, item, cost, payment = data
        print "{0}\t{1}".format(item, cost)

