#!/usr/bin/python

import sys

count = 0
total = 0.0

for line in sys.stdin:

    data = line.strip().split("\t")
    if len(data) != 2:
        continue

    store, amount = data

    total += float(amount)
    count += 1

print "count\t", count
print "total\t", total
