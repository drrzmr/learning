#!/usr/bin/python

import sys

try:
    old_key = sys.stdin.readline().strip()
except:
    sys.exit(0)    

most_key = old_key
most_count = 1
count = 1

for line in sys.stdin:
    key = line.strip()

    # counting keys...
    if key == old_key:
        count += 1
        continue

    # handle most popular
    if count > most_count:
        most_key = old_key
        most_count = count

    # reset counter
    count = 1
    old_key = key

print '{0}\t{1}'.format(most_key, most_count)
