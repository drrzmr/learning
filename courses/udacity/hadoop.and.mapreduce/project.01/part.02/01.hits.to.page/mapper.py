#!/usr/bin/python

# %h is the IP address of the client
# %l is identity of the client, or "-" if it's unavailable
# %u is username of the client, or "-" if it's unavailable
# %t is the time that the server finished processing the request. The format is [day/month/year:hour:minute:second zone]
# %r is the request line from the client is given (in double quotes). It contains the method, path, query-string, and protocol or the request.
# %>s is the status code that the server sends back to the client. You will see see mostly status codes 200 (OK - The request has succeeded), 304 (Not Modified) and 404 (Not Found). See more information on status codes in W3C.org
# %b is the size of the object returned to the client, in bytes. It will be "-" in case of status code 304.
#
# 10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] "GET /assets/js/lowpro.js HTTP/1.1" 200 10469
# %h %l %u %t \"%r\" %>s %b

import sys


class CommonLogLine:

    def __init__(self, line):
        line = line.strip()

        self.ip = ''
        self.identity = ''
        self.username = ''
        self.timestamp = ''
        self.method = ''
        self.path = ''
        self.querystring = ''
        self.protocol = ''
        self.status = 0
        self.size = 0

        try:
            ini, end = line.split(' [', 1)
        except:
            return

        self.timestamp, end = end.split('] "', 1) # timestamp
        self.ip, ini = ini.split(' ', 1) # ini -> identity + username

        request, end = end.split('" ')
        try:
            request = request.split(' ') # ignore: "LIMIT 0"
            self.method = request[0]
            self.path = request[1]
            self.protocol = request[-1]
        except Exception as e:
            raise Exception(str(e), request, line)

        status, size = end.split(' ')
        self.status = int(status)
        self.size = 0 if size == '-' else int(size)

    def __str__(self):
        lst = []
        if any(self.ip):
            lst.append('ip=%s' % self.ip)
        if any(self.identity):
            lst.append('identity=%s' % self.identity)
        if any(self.username):
            lst.append('username=%s' % self.username)
        if any(self.timestamp):
            lst.append('ts=%s' % self.timestamp)
        if any(self.method):
            lst.append('method=%s' % self.method)
        if any(self.path):
            lst.append('path=%s' % self.path)
        if any(self.querystring):
            lst.append('qs=%s' % self.querystring)
        if any(self.protocol):
            lst.append('protocol=%s' % self.protocol)
        if self.status > 0:
            lst.append('status=%d' % self.status)
        if self.size > 0:
            lst.append('size=%d' % self.size)
        
        return ', '.join(lst)
        
    
for line in sys.stdin:

    line = CommonLogLine(line)
    if not line.path:
        continue

    print('{0}\t1'.format(line.path))
