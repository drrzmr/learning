package com.lightbend.training.scalatrain

case class Train(info: TrainInfo, schedule: Seq[(Time, Station)]) {
  require(schedule.length >= 2, "schedule.length must be >= 2")

  val stations: Seq[Station] = schedule.map(_._2)

  def timeAtOld(station: Station): Option[Time] = schedule.find(_._2 == station).map(_._1)

  def timeAt(station: Station): Option[Time] = schedule.find {
    case (_, candidateStation) => candidateStation == station
  }.map {
    case (time, _) => time
  }
}

sealed abstract class TrainInfo {
  def number: Int
}

case class InterCityExpress(number: Int, hasWifi: Boolean = false) extends TrainInfo

case class RegionalExpress(number: Int) extends TrainInfo

case class BavarianRegional(number: Int) extends TrainInfo