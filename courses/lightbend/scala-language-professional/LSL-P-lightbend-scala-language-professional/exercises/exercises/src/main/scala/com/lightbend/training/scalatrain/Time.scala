package com.lightbend.training.scalatrain

import scala.util.Try
import play.api.libs.json._

case class Time(hours: Int = 0, minutes: Int = 0) extends Ordered[Time] {
  require(hours >= 0 && hours < 24, s"hours must be >= 0 and < 24, not $hours")
  require(minutes >= 0 && minutes < 60, s"minutes must be >=0 and < 60, not $minutes")

  val asMinutes: Int = hours * 60 + minutes

  def minus(that: Time): Int = asMinutes - that.asMinutes

  def -(that: Time): Int = minus(that)

  override lazy val toString: String = f"$hours%02d:$minutes%02d"

  override def compare(that: Time): Int = asMinutes - that.asMinutes

  lazy val toJson: JsValue = JsObject(Map(
    "hours" -> JsNumber(hours),
    "minutes" -> JsNumber(minutes)
  ))
}

object Time {
  def fromMinutes(m: Int): Time = Time(m / 60, m % 60)

  def fromJsonV1(json: JsValue): Option[Time] =
    Try((json \ "hours").as[Int]).map { hours =>
      Time(
        hours = hours,
        minutes = Try((json \ "minutes").as[Int]).getOrElse(0)
      )
    }.toOption

  def fromJson(json: JsValue): Option[Time] = {
    for {
      hours <- Try((json \ "hours").as[Int])
      minutes <- Try((json \ "minutes").as[Int]).recover(_ => 0)
    } yield Time(hours, minutes)
  }.toOption
}