package com.rockthejvm.zio.me

import zio.*

import java.io.IOException
import scala.util.{Failure, Success, Try}

object ZIOErrorHandling extends ZIOAppDefault {

  /**
   * Exercise: implement a version of fromTry, fromOption, fromEither, either, absolve
   * using fold and foldZIO
   */

  def fromTry[A](t: Try[A]): Task[A] = t match
    case Failure(exception) => ZIO.fail(exception)
    case Success(value) => ZIO.succeed(value)

  def fromOption[A](o: Option[A]): ZIO[Any, Option[Nothing], A] = o match
    case None => ZIO.fail(None)
    case Some(value) => ZIO.succeed(value)

  def fromEither[E, A](e: Either[E, A]): ZIO[Any, E, A] = e match
    case Left(error) => ZIO.fail(error)
    case Right(value) => ZIO.succeed(value)

  val attemptEffect: Task[String] = ZIO.attempt("attempt effect")

  def either[R, E, A](effect: ZIO[R, E, A]): ZIO[R, Nothing, Either[E, A]] = effect.foldZIO(
    failure = error => ZIO.succeed(Left(error)),
    success = value => ZIO.succeed(Right(value))
  )

  def absolve[R, E, A](zio: ZIO[R, Nothing, Either[E, A]]): ZIO[R, E, A] = zio.flatMap {
    case Left(error) => ZIO.fail(error)
    case Right(value) => ZIO.succeed(value)
  }

  /**
   * Exercises:
   */

  // 1 - make this effect fail with a TYPED error
  val aBadFailure: ZIO[Any, Nothing, Int] = ZIO.succeed[Int](throw new RuntimeException("this is bad!"))
  val betterFailure1: ZIO[Any, Cause[Nothing], Int] = aBadFailure.sandbox
  val betterFailure2: ZIO[Any, Throwable, Int] = aBadFailure.unrefine {
    case exception => exception
  }

  // 2 - transform a zio into another zio with a narrower exception type
  def ioException[R, A](zio: ZIO[R, Throwable, A]): ZIO[R, IOException, A] = zio.refineOrDie[IOException] {
    case exception: IOException => exception
  }

  // 3
  def left[R, E, A, B](zio: ZIO[R, E, Either[A, B]]): ZIO[R, Either[E, A], B] = zio.foldZIO(
    failure = error => ZIO.fail(Left(error)),
    success = value => value match
      case Left(error) => ZIO.fail(Right(error))
      case Right(value) => ZIO.succeed(value)
  )

  // 4
  val database: Map[String, Int] = Map(
    "daniel" -> 123,
    "alice" -> 789
  )

  trait DatabaseError
  case class NotFoundError(data: String) extends DatabaseError
  case class QueryError(reason: String) extends DatabaseError
  case class UserProfile(name: String, phone: Int)

  def lookupProfile(userId: String): ZIO[Any, QueryError, Option[UserProfile]] =
    if (userId != userId.toLowerCase())
      ZIO.fail(QueryError("user ID format is invalid"))
    else
      ZIO.succeed(database.get(userId).map(phone => UserProfile(userId, phone)))

  // surface out all the failed cases of this API
  def betterLookupProfile(userId: String): ZIO[Any, DatabaseError, UserProfile] = {
    if (userId != userId.toLowerCase())
      ZIO.fail(QueryError("user ID format is invalid"))
    else database.get(userId) match
      case None => ZIO.fail(NotFoundError("user ID not found"))
      case Some(phone) => ZIO.succeed(UserProfile(userId, phone))
  }

  override def run = ???
}
