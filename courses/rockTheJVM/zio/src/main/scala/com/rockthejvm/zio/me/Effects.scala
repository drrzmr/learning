package com.rockthejvm.zio.me

import scala.io.StdIn

object Effects {

  case class IO[A](unsafeRun: () => A) {
    def map[B](f: A => B): IO[B] =
      IO.from(f(unsafeRun()))

    def flatMap[B](f: A => IO[B]): IO[B] =
      IO.from(f(unsafeRun()).unsafeRun())
  }

  object IO {
    def from[A](block: => A): IO[A] = IO(unsafeRun = () => block)
  }

  /**
   * Exercises - create some IO which
   *  1. measure the current time of the system
   *  2. measure the duration of a computation
   *    - use exercise 1
   *    - use map/flatMap combinations of MyIO
   *  3. read something from the console
   *  4. print something to the console (e.g. "what's your name"), then read, then print a welcome message
   */

  // 1. measure the current time of the system
  val currentTimeMillis: IO[Long] = IO.from(System.currentTimeMillis())

  // 2. measure the duration of a computation
  def measureDuration[A](computation: IO[A]): IO[(Long, A)] =
    for {
      start <- currentTimeMillis
      a <- computation
      stop <- currentTimeMillis
    } yield (stop - start, a)

  // 3. read something from the console
  val readLine: IO[String] = IO.from(StdIn.readLine())
  def printLine(line: String): IO[Unit] = IO.from(println(line))

  // 4. print something to the console (e.g. "what's your name"), then read, then print a welcome message
  val program: IO[String] = for {
    _ <- printLine("What's your name?")
    name <- readLine
    _ <- IO.from(Thread.sleep(1000))
    _ <- printLine(s"Welcome $name")
  } yield name

  def main(args: Array[String]): Unit = {

    val (duration, name) = measureDuration(program).unsafeRun()
    println(s"name: $name, duration: $duration")
  }
}
