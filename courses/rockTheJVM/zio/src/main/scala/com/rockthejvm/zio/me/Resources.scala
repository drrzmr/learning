package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

import java.util.Scanner
import java.io.File

object Resources extends ZIOAppDefault {

  /**
   * Exercises
   * 1. Use the acquireRelease to open a file, print all lines, (one every 100 millis), then close the file
   */
  def openFileScanner(path: String): UIO[Scanner] =
    ZIO.succeed(new Scanner(new File(path)))

  def read(scanner: Scanner): UIO[Unit] = {
    if (!scanner.hasNext) ZIO.unit
    else {
      for {
        _ <- ZIO.succeed(println(scanner.nextLine()))
        _ <- ZIO.sleep(100.millis)
        ret <- read(scanner)
      } yield ret
    }
  }

  def acquireOpenFile(path: String): UIO[Unit] = {

    val withOpenedFile = ZIO.acquireReleaseWith(
      for {
        _ <- ZIO.succeed(s"> opening $path").debugThread
        scanner = new Scanner(new File(path))
        _ <- ZIO.succeed(s"> opened $path").debugThread
      } yield scanner
    )(
      scanner => for {
        _ <- ZIO.succeed(s"> closing $path").debugThread
        _ = scanner.close()
        _ <- ZIO.succeed(s"> closed $path").debugThread
      } yield ()
    )

    withOpenedFile { scanner => read(scanner) }
  }

  val testInterruptFileDisplay: ZIO[Any, Nothing, Unit] = for {
    fib <- acquireOpenFile("src/main/scala/com/rockthejvm/zio/me/Resources.scala").fork
    _ <- ZIO.sleep(2.seconds) *> fib.interrupt
    _ <- fib.join
  } yield ()

  override def run = testInterruptFileDisplay
}
