package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

object Interruptions extends ZIOAppDefault {

  /** ***********
   * Exercises *
   * *********** */

  /**
   * 1) implement a timeout function
   * - if zio is successful before timeout => a successful effect
   * - if zio fails before timeout => a failed effect
   * - if zio takes longer than timeout => interrupt the effect
   */
  def timeout[R, E, A](zio: ZIO[R, E, A], time: Duration): ZIO[R, E, A] = {
    for {
      fib: Fiber[E, A] <- zio.fork
      _ <- (ZIO.sleep(time) *> fib.interrupt).fork
      result <- fib.join
    } yield result
  }

  /**
   * 2) timeout v2
   * - if zio is successful before timeout => a successful effect with Some(a)
   * - if zio fails before timeout => a failed effect
   * - if zio takes longer than timeout => interrupt the effect, return a successful effect with None
   *
   * hint: foldCauseZIO
   */
  def timeoutOption[R, E, A](zio: ZIO[R, E, A], time: Duration): ZIO[R, E, Option[A]] = {
    timeout(zio, time).foldCauseZIO(
      failure = cause =>
        if (cause.isInterrupted) ZIO.succeed(None)
        else ZIO.failCause(cause),
      success = value => ZIO.succeed(Some(value))
    )
  }

  val waitEffect: ZIO[Any, Nothing, Unit] =
    ZIO.succeed("begin of effect").debugThread *>
      ZIO.sleep(2.second) *>
      ZIO.succeed("end of effect").debugThread *>
      ZIO.unit

  // override def run = timeout(waitEffect, 10.second)
  override def run = timeoutOption(waitEffect, 1.second).debugThread
}
