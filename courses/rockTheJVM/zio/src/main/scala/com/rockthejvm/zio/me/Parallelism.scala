package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils._

object Parallelism extends ZIOAppDefault {

  /**
   * Exercises: do the word counting exercise, using a parallel combinator
   */

  import Fibers.countWorldsOnFile

  val countWordsPar: ZIO[Any, Nothing, Int] = {
    val fnSeq: Seq[String] = (1 to 10).map(i => s"src/main/resources/testFile-$i.txt")
    val countEffectSeq: Seq[ZIO[Any, Throwable, Int]] = fnSeq.map(fn => countWorldsOnFile(fn))
    val collectedEffect: ZIO[Any, Throwable, Seq[Int]] = ZIO.collectAllPar(countEffectSeq)
    val resultEffect: ZIO[Any, Throwable, Int] = collectedEffect.map(partialResults => partialResults.sum)

    resultEffect.orDie
  }

  override def run = countWordsPar.debugThread
}
