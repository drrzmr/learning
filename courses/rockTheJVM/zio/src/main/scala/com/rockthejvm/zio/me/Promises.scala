package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

object Promises extends ZIOAppDefault {

  /**
   * Exercises
   * 1. Write a simulated "egg boiler" with two ZIOs
   *  - one increments a counter every 1s
   *  - one waits for the counter to become 10, after which it will "ring a bell"
   *
   * 2. Write a "race pair"
   *  - use a Promise which can hold an Either[exit for A, exit for B]
   *  - start a fiber for each ZIO
   *  - on completion (with any status), each ZIO needs to complete that Promise
   *    (hint: use a finalizer)
   *  - waiting on the Promise's value can be interrupted!
   *  - if the whole race is interrupted, interrupt the running fibers
   */

  /** 1 */
  val eggBoiler: UIO[Unit] = {

    type Signal = Promise[Nothing, Unit]
    object Signal {
      def make: UIO[Promise[Nothing, Unit]] = Promise.make[Nothing, Unit]
    }

    def timer(signal: Signal, counter: Int): UIO[Unit] = {
      for {
        _ <- ZIO.sleep(1.second)
        _ <- ZIO.succeed(s"counter == $counter").debugThread
        _ <- if (counter == 10) signal.succeed(()) else timer(signal, counter + 1)
      } yield ()
    }

    def alarm(signal: Signal): UIO[Unit] = {
      for {
        _ <- ZIO.succeed("> wait egg to be ready").debugThread
        _ <- signal.await
        _ <- ZIO.succeed("> TRIIIIMMM").debugThread
      } yield ()
    }

    for {
      signal <- Signal.make
      _ <- timer(signal, 0) zipPar alarm(signal)
    } yield ()
  }

  /** 2 */
  type ExitA[E, A, B] = (Exit[E, A], Fiber[E, B])
  type ExitB[E, A, B] = (Fiber[E, A], Exit[E, B])
  type RacePairResult[E, A, B] = Either[ExitA[E, A, B], ExitB[E, A, B]]

  def racePair[R, E, A, B](zioA: => ZIO[R, E, A], zioB: ZIO[R, E, B]): ZIO[R, Nothing, RacePairResult[E, A, B]] = {

    val result: ZIO[R, Nothing, RacePairResult[E, A, B]] = ZIO.uninterruptibleMask { restore =>
      for {
        signal <- Promise.make[Nothing, Either[Exit[E, A], Exit[E, B]]]
        _ <- ZIO.succeed(">>> signal created").debugThread
        fibA <- zioA.onExit(exitA => signal.succeed(Left(exitA))).fork
        fibB <- zioB.onExit(exitB => signal.succeed(Right(exitB))).fork

        result <- restore(signal.await).onInterrupt {
          for {
            intA <- fibA.interrupt.fork
            intB <- fibB.interrupt.fork
            _ <- intA.join
            _ <- intB.join
          } yield ()
        }

      } yield result match
        case Left(exitA) => Left((exitA, fibB))
        case Right(exitB) => Right((fibA, exitB))
    }

    result
  }

  val runRacePair: UIO[Unit] = {
    val zioA: UIO[Int] = ZIO.sleep(3.second).as(3).onInterrupt(ZIO.succeed("zio A interrupted").debugThread)
    val zioB: UIO[Int] = ZIO.sleep(2.second).as(2).onInterrupt(ZIO.succeed("zio B interrupted").debugThread)

    for {
      _ <- ZIO.succeed(">>> let's start the race!").debugThread
      raceResult: RacePairResult[Nothing, Int, Int] <- racePair(zioA, zioB)
      _ <- raceResult match
        case Left((exitA, fibB)) => fibB.interrupt *> ZIO.succeed(s">>> zio A is the winner: $exitA").debugThread
        case Right((fibA, exitB)) => fibA.interrupt *> ZIO.succeed(s">>> zio B is the winner: $exitB").debugThread
    } yield ()
  }

  // override def run = eggBoiler
  override def run = runRacePair
}
